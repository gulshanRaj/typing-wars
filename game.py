#
# Typing-wars
# A simple typing game
# Author: Gulshan Raj

VERSION = "0.1"
"""Goals:
-  Player represented by rectangles
-  Projectiles represented by rectangles
-  Physical type attack
-  Health bar for each player
-  Projectiles can collide
-  Projectiles destroyed on collision with player
-  Projectiles lose energy on collision
-  Click to launch projctile"""

try:
    import sys
    import random
    import math
    import os
    import pygame
    from socket import *
    from pygame.locals import *
except ImportError:
    print("couldn't load module.")
    sys.exit(2)

def load_image(name):
    """ Load image and return image object"""
    fullname = os.path.join('data', name)
    try:
        image = pygame.image.load(fullname)
        if image.get_alpha is None:
            image = image.convert()
        else:
            image = image.convert_alpha()
    except pygame.error as message:
        print('Cannot load image:', fullname)
        raise SystemExit
    return image, image.get_rect()

def load_sound(name):
    class NoneSound:
        def play(self): pass
    if not pygame.mixer or not pygame.mixer.get_init():
        return NoneSound()
    fullname = os.path.join(data_dir, name)
    try:
        sound = pygame.mixer.Sound(fullname)
    except pygame.error:
        print ('Cannot load sound: %s' % fullname)
        raise SystemExit(str(geterror()))
    return sound

class Ball(pygame.sprite.Sprite):
    """A ball that players use for attacking
    Returns: ball object
    Functions: 
    Attributes: rect, mass, speed, power, bid"""

    bidno = 0  #used to give unique bid to a ball
    def __newBidno__(self):
        Ball.bidno = Ball.bidno+1
        return Ball.bidno

    def __getIndexOfBall__(ball):
        for idx in range(0, len(balls)):
            if balls[idx].bid == ball.bid:
                return idx

    def __init__(self, player, speed):
        pygame.sprite.Sprite.__init__(self)
        self.image, self.rect = load_image('redRectangle.png')
        self.mass = 3
        self.bid = self.__newBidno__()
        #self.nprect = np.array(self.rect)
        if player.side == "left":
            self.rect.midleft = player.rect.midright
            self.speed = speed
        elif player.side == "right":
            self.rect.midright = player.rect.midleft
            self.speed = [-3, 0]           #TODO use matrix op
        self.power = self.getPower()

    def update(self):
        #check collision
        idx = self.rect.collidelist(balls)
        if idx != -1 and balls[idx].bid != self.bid:
            mv1 = self.mass*self.speed[0]
            mv2 = balls[idx].mass*balls[idx].speed[0]
            totalmv = mv1+mv2
            totalm = self.mass+balls[idx].mass
            speed = [totalmv//totalm, 0]
            #remove old balls
            ballsprites.remove(balls[idx])
            balls.pop(idx)
            #ballsprites.remove(balls[idx])
            if speed[0] == 0: 
                ballsprites.remove(self)
                myidx = self.__getIndexOfBall__()
                balls.pop(myidx)
            else:
                self.speed = speed
                self.mass = totalm
            return
        
        #in usual case
        #self.nprect = self.nprect + self.speed
        self.rect = self.rect.move(self.speed)
    
    def handleBallCollision(self, ball2):
        #momentum conservation, inelastic collision
        
        return totalm, speed
                
    def getPower(self):
        return self.mass*self.speed[0]*self.speed[0]
    
    def removeLastFrame(self):
        screen.blit(background, self.rect, self.rect)
        

class Player(pygame.sprite.Sprite):
    """Player
    Returns: Player object
    Functions: 
    Attributes: area, health, side"""

    def __init__(self, side):
        pygame.sprite.Sprite.__init__(self)
        self.image, self.rect = load_image('player.png')
        #screen = pygame.display.get_surface()
        self.side = side
        self.health = 100
        self.healthRect = Rect(0, 0, 0, 0)

        if side == "right":
            self.rect.midright = screen.get_rect().midright
            self.healthRect = Rect(screen.get_rect().centerx, screen.get_rect().bottom-10, self.health, 8)      #hardcoded

        elif side == "left":
            self.rect.midleft = screen.get_rect().midleft
            self.healthRect = Rect(screen.get_rect().left, screen.get_rect().bottom-10, self.health, 8)      #hardcoded
    
    def removeLastFrame(self):
        screen.blit(background, self.rect, self.rect)
        screen.blit(background, self.healthRect, self.healthRect)
    
    def update(self):
        idx = self.rect.collidelist(balls)
        if idx == -1:
            return
        else:
            self.health -= balls[idx].power
            ballsprites.remove(balls[idx])
            balls.pop(idx)
            
                
    def drawHealthBar(self):
        x = self.health
        healthColor = (255*(100-x)//100, 255*x//100, 0)
        self.healthRect.width = x
        pygame.draw.rect(screen, healthColor, self.healthRect, 0)
        
def main():
    # Initialise screen
    pygame.init()
    global screen
    screen = pygame.display.set_mode((800, 600))
    pygame.display.set_caption('Typing Wars')

    # Fill background
    global background
    background = pygame.Surface(screen.get_size())
    background = background.convert()
    background.fill((0, 0, 0))

    # Initialise players
    global ballsprites
    global balls
    playerLeft = Player("left")
    playerRight = Player("right")
    balls = []
    # Initialise sprites
    playersprites = pygame.sprite.RenderPlain((playerLeft, playerRight))
    ballsprites = pygame.sprite.RenderPlain(())
    
    # Blit everything to the screen
    screen.blit(background, (0, 0))
    pygame.display.flip()

    # Initialise clock
    clock = pygame.time.Clock()

    # Event loop
    while 1:
        # Make sure game doesn't run at more than 60 frames per second
        clock.tick(60)
        
        for event in pygame.event.get():
            if event.type == QUIT:
                return
            #wsd for left_player up,down,left for right_player
            elif event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    return
                elif event.key == K_LEFT:
                    ball = Ball(playerRight, [-3,0])
                    balls.append(ball)
                    ballsprites = pygame.sprite.RenderPlain(balls)
                elif event.key == K_d:
                    ball = Ball(playerLeft, [3,0])
                    balls.append(ball)
                    ballsprites = pygame.sprite.RenderPlain(balls)
                
        #clearing previous data
        playerLeft.removeLastFrame()
        playerRight.removeLastFrame()
        for ball in balls:
            ball.removeLastFrame()
        
        playersprites.update()
        ballsprites.update()
        
        playersprites.draw(screen)
        ballsprites.draw(screen)
        
        #HealthBars
        playerLeft.drawHealthBar()
        playerRight.drawHealthBar()
        pygame.display.flip()


if __name__ == '__main__': main()