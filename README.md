This is a minimilastic typing game built in python. It has 2 players who can move up and down and shoot projectiles at the enemy. All actions are performed using explicit commands.

Goals:
#Version 0.1
- Player represented by rectangles
- Projectiles represented by rectangles
- Physical type attack
- Health bar for each player
- Projectiles can collide
- Projectiles destroyed on collision with player
- Projectiles lose energy on collision

Building instructions

Requirements:
1. Python 3
2. pygame

Instructions:
1. Clone the repository.
2. cd typing-wars
3. python3 game.py